# Books

记录一些编程书籍资源 - 学习不缺资源，才能快乐的学习


- [PHP程序员进阶书籍](https://github.com/phpsb/book)
- [程序人生 阅读快乐](https://www.yuque.com/winforlife/vgzph9) 编程开发的基础知识
- [大杂烩编程书籍网](https://www.bibiqiqi.com/)
- [各种编程语言书 - 最关键的是有数学方面的教材](https://github.com/KeKe-Li/book)
- [一样是一些高等数学和程序设计的书](https://github.com/bumzy/book)
- [四本数学方面的书](https://github.com/jcnlp/books/tree/master/math)
- [设计模式和离散数学的书](https://github.com/Owen864720655/E-book)
- [各种编程语言的思维导图 - 包括数学](https://github.com/yejinlei/about)
- [浙江大学课程攻略共享计划](https://github.com/QSCTech/zju-icicles)
- [计算机工程以及数学相关](https://github.com/yuanliangding/books)
- [上海交通大学课程分享](https://github.com/CoolPhilChen/SJTU-Courses)
- [一些课程资料共享](https://github.com/gogoforit/university-courses)
- [一些编程文档](https://github.com/caijyi1/PDF) 高重量的 Linux Python Nginx